# wordpress構築ワークショップ

## 前提条件
- virtualboxインストール済み
- Vagrantインストール済み

## 使い方
このフォルダをダウンロードします。基本的にはzipでOK  
その後ターミナル上でVagrant/centos8ディレクトリに移動しvagrantを利用してcentos8の仮想PCを起動する。

```
# windows
cd Vagrant\centos8
vagrant up

# linux
cd Vagrant/centos8
vagrant up
```

Wikiの手順に従い、wordpressを構築します。  
[Wordpress構築研修](https://gitlab.com/hideaki_hara/itc-work/-/wikis/Wordpress%E6%A7%8B%E7%AF%89%E7%A0%94%E4%BF%AE)

## 工事中
#### Vagrant/lamp-centos8について
直接VagrantやAnsible等でWordpressの構築自動化を作ってみる。